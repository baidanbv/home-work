1. Опишіть своїми словами як працює метод forEach.

2. Як очистити масив?

3.  Як можна перевірити, що та чи інша змінна є масивом?

1. Ми передаємо callback функцію в метод forEach і даємо їй один аргумент який буде являтися елементом масиву до якого ми його приміняємо, і на кожній новій ітерації наш елемент буде ставати наступним елементом масиву, поки ми повністю не пройдемо по всьому масиву

2. Це можна зробити через метод arr.splise(), або присвоїти 0 довжині масиву

3. Це можна зробити через глобальний об'єкт Array.isArray(наш масив)