
/****Ammount images to show****/
const windowResize = () => {
const body = document.querySelector('body');
const changeInstaTitle = document.querySelector('.title_link h3.global_title')
let windowWidth = body.clientWidth;

    if(windowWidth <= 767) {
        changeInstaTitle.textContent = 'Last Instagram Shot';
    } else {
        changeInstaTitle.textContent = 'Latest Instagram Shots';
    }

   }

window.addEventListener('resize', windowResize);

/****Burger menu****/
const burgerMenu = document.querySelector('.mobile_nav');
const navBar = document.querySelector('.header nav');

const burgerMenuIcon = document.querySelector('.mobile_nav img');
const burgerIconUrl =  './dist/assets/img/mobile_menu.svg';
const closeIconUrl = './dist/assets/img/close_icon.svg';
let toggle = true;

const toggleIcon = () => {
    toggle = !toggle;
    if (toggle) {
      burgerMenuIcon.src = burgerIconUrl;
    } else {
      burgerMenuIcon.src = closeIconUrl;
    }
}
burgerMenu.addEventListener('click', () => {
    navBar.classList.toggle('active_bar')
    toggleIcon()
})