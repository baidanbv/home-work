const images = document.querySelectorAll('.image-to-show');
const stopButton = document.getElementById('stopButton');
const resumeButton = document.getElementById('resumeButton');

let currentIndex = 0;
let intervalId = null;
    
    const showImage = (index) => {
      images.forEach(function(image) {
        image.classList.remove('active');
      });
      
      images[index].classList.add('active');
    }
    
    const startSlideShow = () => {
      intervalId = setInterval(() => {
        currentIndex = (currentIndex + 1) % images.length;
        showImage(currentIndex);
      }, 3000);
    }
    
    const stopSlideShow = () => {
      clearInterval(intervalId);
    }
    
    const resumeSlideShow = () => {
      startSlideShow();
    }
    
    startSlideShow();
    
    stopButton.addEventListener('click', () => {
        stopSlideShow();
    });
    
    resumeButton.addEventListener('click', () => {
      resumeSlideShow();
    });