
// let secondNumber = prompt('Enter your second number')
// let operationSymbol = prompt('Enter your operation symbol, It`s must be "+", "-", "*" or "/"')
let result = '';

const firstNumber = checkNumber('Enter your first number')
const secondNumber = checkNumber('Enter your second number')
const operationSymbol = checkSymbol('Enter your operation symbol, It`s must be "+", "-", "*" or "/"')
function checkNumber(numberMessage)  {
    let number = prompt(numberMessage)
    
    while(number.trim() === "" && isNaN(number)) {
        number = parseInt(prompt(numberMessage))
     }
     return number
}

function checkSymbol(symbolMessage) {
    let symbol = prompt(symbolMessage)

    while(symbol !== '+' && symbol !== '-' && symbol !== '*' && symbol !== '/') {
        symbol = prompt(symbolMessage)
    }
    return symbol
}
  const mathOperation = (num1,num2,operationSymbol) => {
    console.log(operationSymbol)
      switch(operationSymbol) {
        case "+":
            result  = parseInt(num1) + parseInt(num2);
            console.log(`${num1} + ${num2} =  ${result}`);
            break;
        case "-":
            result = parseInt(num1) - parseInt(num2);
            console.log(`${num1} - ${num2} =   ${result}`);
            break;
        case "*":
            result = parseInt(num1) * parseInt(num2);
            console.log(`${num1} * ${num2} =   ${result}`);
            break;
        case "/":
            result = parseInt(num1) / parseInt(num2);
            console.log(`${num1} / ${num2} =   ${result}`);
            break;
        default:
            alert('You have to use just one of this operations "+", "-", "*" or "/", Try again!')  
      }
    }
    console.log(mathOperation(firstNumber,secondNumber,operationSymbol))


