  const createNewUser = () => {
  const firstName = prompt('Enter your first name')
  const lastName = prompt('Enter your last name')
  const now = new Date()
  const currentDate = `${now.getDate()}.${now.getMonth()}.${now.getFullYear()}`;
  const birthday =  prompt(`Enter your date of birth in this format ${currentDate}`, '05.05.1995')
  const userBirthYear = new Date(birthday);
  let userAge = now.getFullYear() - userBirthYear.getFullYear()
  const monthDiff = now.getMonth() - userBirthYear.getMonth();
  const  newUser = {
    _firstName: firstName,
    _lastName: lastName,
    set firstName(name) {
      if(name === "Ivan") {
        this._firstName = name;
      }
    },
    set lastName(surName) {
      if(surName === "Kravchenko") {
        this._lastName = surName;
      }
    },
    getLogin()  {
     const userLogin = this._firstName[0] + this._lastName

     return `User login is: ${userLogin.toLowerCase()}`
    },
    getAge() {
      if (monthDiff < 0 || (monthDiff === 0 && now.getDate() < userBirthYear.getDate())) {
        userAge--;
      }
      return  `User age is:  ${userAge}`
    },
    getPassword() {
      const userPassword = this._firstName[0].toUpperCase() + this._lastName.toLowerCase() + userBirthYear.getFullYear()
  
     return `User password is: ${userPassword}`
    }
  }
  console.log(newUser.getLogin())
  console.log(newUser.getPassword())
  console.log(newUser.getAge())
  return newUser;
}
const user = createNewUser()
console.log(user)


  
