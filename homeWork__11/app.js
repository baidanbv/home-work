const IconShowPass1 = document.getElementById('show-pass_1');
const IconShowPass2 = document.getElementById('show-pass_2');
const passwordOne = document.getElementById('password-1');
const passwordTwo = document.getElementById('password-2');
const error = document.getElementById('error');

    

    const togglePasswordVisibility = (event) => {
        const targetInput = event.target.previousElementSibling;
        const targetIcon = event.target;
        
        if (targetInput.type === 'password') {
            targetInput.type = 'text';
            targetIcon.classList.remove('fa-eye');
            targetIcon.classList.add('fa-eye-slash');
        } else {
            targetInput.type = 'password';
            targetIcon.classList.remove('fa-eye-slash');
            targetIcon.classList.add('fa-eye');
        }
    }

    const validatePasswords = (event) => {
        event.preventDefault(); 
        
        if (passwordOne.value === passwordTwo.value) {
            alert('You are welcome');
        } else {
            error.textContent = 'Потрібно ввести однакові значення';
        }
    }

    IconShowPass1.addEventListener('click', togglePasswordVisibility);
    IconShowPass2.addEventListener('click', togglePasswordVisibility);
    document.querySelector('form').addEventListener('submit', validatePasswords);