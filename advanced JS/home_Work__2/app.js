// Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).

// На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).

// Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих     властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.

// Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

const books = [
  {
    author: 'Люсі Фолі',
    name: 'Список запрошених',
    price: 70,
  },
  {
    author: 'Сюзанна Кларк',
    name: 'Джонатан Стрейндж і м-р Норрелл',
  },
  {
    name: 'Дизайн. Книга для недизайнерів.',
    price: 70,
  },
  {
    author: 'Алан Мур',
    name: 'Неономікон',
    price: 70,
  },
  {
    author: 'Террі Пратчетт',
    name: 'Рухомі картинки',
    price: 40,
  },
  {
    author: 'Анґус Гайленд',
    name: 'Коти в мистецтві',
  },
];

const root = document.querySelector('#root');
const ul = document.createElement('ul');

root.appendChild(ul);

try {
  const filterBooks = books.filter(
    (item) => item.author && item.price && item.name
  );
  books.map((item, index) => {
    if (!Object.hasOwn(item, 'author')) {
      console.error(`Can't find key: "author" in object ${index + 1}`);
      // throw new Error(`Can't find key: "author" in object ${index + 1}`);
    } else if (!Object.hasOwn(item, 'price')) {
      console.error(`Can't find key: "price" in object ${index + 1}`);
      // throw new Error(`Can't find key: "price" in object ${index + 1}`);
    }
  });

  filterBooks.map((item) => {
    const li = document.createElement('li');
    for (const [key, value] of Object.entries(item)) {
      li.innerHTML += `<span>${key}</span>: ${value};  `;

      const wrapperKey = li.querySelectorAll('span');
      wrapperKey.forEach((item) => (item.style.color = 'red'));

      ul.appendChild(li);
    }
  });
} catch (e) {
  console.log(e.message);
}
