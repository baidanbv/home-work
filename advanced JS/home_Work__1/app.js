class Visit {
  constructor(doctor, purpose, description, urgency, name) {
    this.doctor = doctor;
    this.purpose = purpose;
    this.description = description;
    this.urgency = urgency;
    this.name = name;
  }
}

class VisitDentist extends Visit {
  constructor(doctor, purpose, description, urgency, name, lastVisitDate) {
    super(doctor, purpose, description, urgency, name);
    this.lastVisitDate = lastVisitDate;
  }
}

class VisitCardiologist extends Visit {
  constructor(
    doctor,
    purpose,
    description,
    urgency,
    name,
    bloodPressure,
    bmi,
    heartConditions,
    age
  ) {
    super(doctor, purpose, description, urgency, name);
    this.bloodPressure = bloodPressure;
    this.bmi = bmi;
    this.heartConditions = heartConditions;
    this.age = age;
  }
}

class VisitTherapist extends Visit {
  constructor(doctor, purpose, description, urgency, name, patientAge) {
    super(doctor, purpose, description, urgency, name);
    this.patientAge = patientAge;
  }
}

class AppointmentModal {
  constructor() {
    this.modal = document.getElementById('appointmentModal');
    this.closeButton = document.getElementById('closeButton');
    this.createButton = document.getElementById('createButton');
    this.doctorSelect = document.getElementById('doctorSelect');
    this.cardiologistFields = document.getElementById('cardiologistFields');
    this.dentistFields = document.getElementById('dentistFields');
    this.therapistFields = document.getElementById('therapistFields');

    this.setupEventListeners();
  }

  showModal() {
    this.modal.style.display = 'block';
  }

  closeModal() {
    this.modal.style.display = 'none';
  }

  handleDoctorSelect() {
    const selectedDoctor = this.doctorSelect.value;

    this.cardiologistFields.style.display = 'none';
    this.dentistFields.style.display = 'none';
    this.therapistFields.style.display = 'none';

    if (selectedDoctor === 'cardiologist') {
      this.cardiologistFields.style.display = 'block';
    } else if (selectedDoctor === 'dentist') {
      this.dentistFields.style.display = 'block';
    } else if (selectedDoctor === 'therapist') {
      this.therapistFields.style.display = 'block';
    }
  }

  handleCreateButtonClick() {
    const selectedDoctor = this.doctorSelect.value;
    const purpose = document.getElementById('purpose').value;
    const description = document.getElementById('description').value;
    const urgency = document.getElementById('urgency').value;
    const name = document.getElementById('name').value;
    let visit;

    if (selectedDoctor === 'cardiologist') {
      const bloodPressure = document.getElementById('bloodPressure').value;
      const bmi = document.getElementById('bmi').value;
      const heartConditions = document.getElementById('heartConditions').value;
      const age = document.getElementById('age').value;
      visit = new VisitCardiologist(
        selectedDoctor,
        purpose,
        description,
        urgency,
        name,
        bloodPressure,
        bmi,
        heartConditions,
        age
      );
    } else if (selectedDoctor === 'dentist') {
      const lastVisitDate = document.getElementById('lastVisitDate').value;
      visit = new VisitDentist(
        selectedDoctor,
        purpose,
        description,
        urgency,
        name,
        lastVisitDate
      );
    } else if (selectedDoctor === 'therapist') {
      const patientAge = document.getElementById('patientAge').value;
      visit = new VisitTherapist(
        selectedDoctor,
        purpose,
        description,
        urgency,
        name,
        patientAge
      );
    }

    // Відправити дані на сервер (AJAX запит)
    // Тут ви повинні реалізувати відправку даних на сервер за допомогою AJAX

    // Після успішної відправки, закрити модальне вікно
    this.closeModal();
  }

  setupEventListeners() {
    this.closeButton.addEventListener('click', () => this.closeModal());
    this.doctorSelect.addEventListener('change', () =>
      this.handleDoctorSelect()
    );
    this.createButton.addEventListener('click', () =>
      this.handleCreateButtonClick()
    );

    // Закрити модальне вікно при кліку поза ним
    window.addEventListener('click', (event) => {
      if (event.target === this.modal) {
        this.closeModal();
      }
    });
  }
}

// Створити екземпляр класу і відобразити модальне вікно
const appointmentModal = new AppointmentModal();
