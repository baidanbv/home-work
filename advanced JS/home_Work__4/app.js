const showLoadingSpinner = (parent) => {
  const loadingSpinner = document.createElement('div');
  loadingSpinner.classList.add('loading-spinner');
  parent.appendChild(loadingSpinner);
};

const hideLoadingSpinner = (parent) => {
  const loadingSpinner = document.querySelector('.loading-spinner');
  if (loadingSpinner) {
    parent.removeChild(loadingSpinner);
  }
};

const showFilmsInfo = (data) => {
  const cards = document.querySelector('.cards');

  data.forEach((item) => {
    const { episodeId, name, openingCrawl, characters } = item;
    const card = document.createElement('div');
    card.classList.add('card');

    card.innerHTML = `
      <div class="card__episode"><h3>Епізод: </h3> ${episodeId}</div>
      <div class="card__film-name"><h3>Назва фільму: </h3> ${name}</div>
      <div class="card__characters"><h3>Персонажі: </h3> </div>
      <div class="card__short-script"><h3>Короткий зміст: </h3> ${openingCrawl}</div>
      `;

    const charactersContainer = card.querySelector('.card__characters');

    showLoadingSpinner(charactersContainer);

    Promise.all(
      characters.map((url) => fetch(url).then((response) => response.json()))
    )
      .then((characters) => {
        characters.forEach((character) => {
          const characterName = character.name;
          const characterWrapper = document.createElement('span');
          characterWrapper.innerText = characterName + ', ';
          charactersContainer.appendChild(characterWrapper);
        });
      })

      .catch((error) => console.log('Помилка при отриманні персонажів:', error))
      .finally(() => {
        hideLoadingSpinner(charactersContainer);
      });

    cards.appendChild(card);
  });
};

fetch('https://ajax.test-danit.com/api/swapi/films')
  .then((response) => response.json())
  .then((data) => {
    const films = data.sort((a, b) => a.episodeId - b.episodeId);
    showFilmsInfo(data);
  })
  .catch((error) =>
    console.log('Помилка при отриманні списку фільмів:', error)
  );
