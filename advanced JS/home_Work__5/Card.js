export class Card {
  constructor(
    authorImg,
    authorName,
    authorEmail,
    authorPostTitle,
    authorPostText,
    postId
  ) {
    this.authorImg = authorImg;
    this.authorName = authorName;
    this.authorEmail = authorEmail;
    this.authorPostTitle = authorPostTitle;
    this.authorPostText = authorPostText;
    this.postId = postId;
  }

  renderPost() {
    const post = `<div class="post" data-post-id="${this.postId}">
            <span class="post__remove-post">Remove Post</span>
            <span class="post__remove-post--edit">Edit Post</span>
            <div class="post__data">
                <img src="${this.authorImg}" alt="">
                <div class="post__author">
                    <div class="post__author-name">${this.authorName}</div>
                    <div class="post__author-email">${this.authorEmail}</div>
                </div>
            </div>
            <div class="post__content">
                <h3 class="post__title">${this.authorPostTitle}</h3>
                <p class="post__text">${this.authorPostText}</p>
            </div>
        </div>`;

    const posts = document.querySelector('#posts');
    posts.innerHTML += post;

    const removeButtons = document.querySelectorAll('.post__remove-post');

    removeButtons.forEach((btn) => {
      btn.addEventListener('click', (e) => {
        const postId = btn.closest('.post').getAttribute('data-post-id');
        this.deletePost(postId);
      });
    });

    return post;
  }

  async deletePost(postId) {
    try {
      const response = await fetch(
        `https://ajax.test-danit.com/api/json/posts/${postId}`,
        { method: 'DELETE' }
      );

      if (response.ok) {
        console.log(`Post with ID ${postId} has been deleted.`);
        const postElement = document.querySelector(
          `[data-post-id="${postId}"]`
        );

        postElement.remove();
      } else {
        console.error(`Failed to delete post with ID ${postId}.`);
      }
    } catch (e) {
      console.error('An error occurred while deleting the post:', e);
    }
  }
}
