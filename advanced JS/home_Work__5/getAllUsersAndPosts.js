import { Card } from "./Card.js";



export const fetchUsers = async () => {
    try {
      const usersUrl = 'https://ajax.test-danit.com/api/json/users';
      const postsUrl = 'https://ajax.test-danit.com/api/json/posts';
      const imgUrl = 'https://ajax.test-danit.com/api/json/albums/1/photos';
    const loader = document.querySelector('.loader-wrapper');
    loader.classList.remove('hide');

    const [responseUsers, responsePosts, responseImg] = await Promise.all([
      fetch(usersUrl),
      fetch(postsUrl),
      fetch(imgUrl),
    ]);
    const images = await responseImg.json();
    const users = await responseUsers.json();
    const posts = await responsePosts.json();

    loader.classList.add('hide');

    posts.forEach((post) => {
      const user = users.find((user) => user.id === post.userId);
      const userImg = images.find((img) => img.id === user.id);
      if (user) {
        const card = new Card(
          userImg.url,
          user.name,
          user.email,
          post.title,
          post.body,
          post.id
        );
        card.renderPost();
      }
    });
  } catch (e) {
    console.log('Error', e);
  }
};


