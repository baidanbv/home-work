import { Card } from "./Card.js";

export const addNewPost = () => {
    const addPostBtn = document.querySelector('.add-post');
    const closePopup = document.querySelector('.close');
    const overlayPopup = document.querySelector('.modal-popup');
    const modalPopup = document.querySelector('.popup');

    const popupForm = document.querySelector('.popup__form');
    const addNewPostBtn = document.querySelector('.popup__add-new-post');
    const postTitle = document.querySelector('#postTitle');
    const postText = document.querySelector('#postText');

    document.addEventListener('click', (event) => {
      if (!modalPopup.contains(event.target) && event.target !== addPostBtn) {
        overlayPopup.classList.remove('show');
      }

      switch (event.target) {
        case addPostBtn:
          overlayPopup.classList.add('show');
          document.body.style.overflow = 'hidden';
          break;
        case closePopup:
          overlayPopup.classList.remove('show');
          document.body.style.overflow = 'initial';
          break;
      }
    });

    popupForm.addEventListener('click', async (event) => {
      event.preventDefault();
      if (event.target === addNewPostBtn) {
        if (
          postAuthor.value !== '' &&
          postAuthorEmail.value !== '' &&
          postTitle.value !== '' &&
          postText.value !== ''
        ) {
          try {
            const response = await fetch(
              'https://ajax.test-danit.com/api/json/posts',
              {
                method: 'POST',
                body: JSON.stringify({
                  userId: 1,
                  name: postAuthor.value,
                  email: postAuthorEmail.value,
                  title: postTitle.value,
                  body: postText.value,
                }),
                headers: { 'Content-Type': 'application/json' },
              }
            );

            if (response.ok) {
              const result = await response.json();
              const postsWrapper = document.getElementById('posts');
              const newPostUrl =
                'https://png.pngtree.com/png-vector/20221018/ourmid/pngtree-twitter-social-media-round-icon-png-image_6315985.png';
              const card = new Card(
                newPostUrl,
                postAuthor.value,
                postAuthorEmail.value,
                postTitle.value,
                postText.value,
                result.id
              );
              postsWrapper.insertAdjacentHTML('afterbegin', card.renderPost());
            }
            popupForm.reset();
            overlayPopup.classList.remove('show');
            document.body.style.overflow = 'initial';
          } catch (e) {
            console.log('Erorr', e);
          }
        } else {
          alert('All fields must be filled');
        }
      }
    });
}
