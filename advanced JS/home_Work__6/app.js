const btnGetIp = document.querySelector('#findByIp');

const getIp = async () => {
  try {
    const response = await fetch('https://api.ipify.org/?format=json');
    const data = await response.json();

    return data.ip;
  } catch (e) {
    console.log('Error', e);
  }
};

const getDataByIp = async (ip) => {
  try {
    const customFields = [
      'continent',
      'country',
      'region',
      'city',
      'district',
      'query',
    ].join(',');

    const response = await fetch(
      `http://ip-api.com/json/${ip}?fields=${customFields}`
    );
    const data = await response.json();

    return data;
  } catch (e) {
    console.log('Error', e);
  }
};

const showIpData = async () => {
  try {
    const ipAddress = await getIp();
    if (ipAddress) {
      const ipData = await getDataByIp(ipAddress);
      if (ipData) {
        const ipDtaElement = document.createElement('div');
        console.log(ipData);
        ipDtaElement.innerHTML = `<span>Континент: ${ipData.continent}</span>
                                <span>Країна: ${ipData.country}</span>
                                <span>Регіон: ${ipData.region}</span>
                                <span>Місто: ${ipData.city}</span>
                                <span>Район: ${ipData.district}</span>`;
        document.body.appendChild(ipDtaElement);
      }
    }
  } catch (e) {
    console.log('Error', e);
  }
};

btnGetIp.addEventListener('click', showIpData);
