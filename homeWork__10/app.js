const tabsTitle = document.querySelectorAll(".tabs-title");
const tabsContent = document.querySelectorAll(".tabs-content li");

tabsTitle.forEach((title, index) => {
    title.dataset.contentId = index + 1;
  title.addEventListener("click", () => {
    tabsTitle.forEach((title) => {
      if (title.classList.contains("active")) {
        title.classList.remove("active");
      }
    });
    tabsContent.forEach((item, index) => {
        item.setAttribute('id', index + 1)
        if (item.classList.contains("active-content")) {
          item.classList.remove("active-content");
        }
      });
    tabsContent.forEach((item) => {
      if (title.getAttribute("data-content-id") === item.getAttribute("id")) {
        item.classList.add("active-content");
      }
    });
    title.classList.add("active");
  });
});
