const p = document.querySelectorAll("p");
p.forEach(el => el.style.color = '#ff0000');

const optionsList = document.getElementById("optionsList");
console.log(optionsList);
console.log(optionsList.parentElement);

const children = optionsList.childNodes;
children.forEach(child => console.log(child.nodeName, child.nodeType));

const testParagraph = document.getElementById("testParagraph");
testParagraph.innerText = "This is a paragraph";

const mainHeader = document.querySelector(".main-header");
const childrenMain = [...mainHeader.children];

childrenMain.forEach(child => child.classList.add("nav-item"));
console.log(childrenMain);

const sectionTitle = document.querySelectorAll(".section-title");
sectionTitle.forEach(title => title.classList.remove("section-title"))


