const parentElement = document.getElementById('wrapper');

const printArray = (arr, parent = document.body) => {
  const ul = document.createElement("ul");
  arr.forEach((element) => {
    const li = document.createElement("li");
    if (Array.isArray(element)) {
     printArray(element,li);
    } else {
      li.textContent = element;
    }
    ul.appendChild(li);
  });

  parent.appendChild(ul)
};

printArray(["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"]);

function clearPage() {
  document.body.innerHTML = '';
}
const timerWrapper = document.createElement("div");
const time = document.createElement("span");
let secondsLeft = 3;

document.body.appendChild(timerWrapper)
timerWrapper.appendChild(time)
timerWrapper.prepend('Page will be clear about ')

time.style.cssText = 'color: red; font-size: 27px; font-weight: bold;'

setInterval(() => {
  time.textContent = secondsLeft;
  secondsLeft--;
  if (secondsLeft < 0) {
    clearPage();
  }
}, 1000);