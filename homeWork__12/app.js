const buttons = document.querySelectorAll('.btn');
    
    document.addEventListener('keydown', event => {
      const key = event.key.toUpperCase();
      buttons.forEach( button => {
        if (button.innerText.toUpperCase() === key) {
          button.classList.add('active');
        } else {
          button.classList.remove('active');
        }
      });
    });