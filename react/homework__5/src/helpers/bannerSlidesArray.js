export const bannerSlidesArray = [
    {
        id: 1,
        category: 'T-shirt / Tops',
        imgUrl: '/bannerShopPage/slide1.jpg',
        title: 'Summer Value Pack',
        description: 'cool / colorful / comfy'
    },
    {
        id: 2,
        category: 'Hoodies & Sweetshirt',
        imgUrl: './bannerShopPage/slide2.jpg',
        title: 'Spring Best Pack',
        description: 'hot / comfy'
    }
];
