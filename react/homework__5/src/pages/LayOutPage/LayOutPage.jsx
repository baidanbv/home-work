import { Outlet } from 'react-router-dom';
import Header from '../../components/Header/Header';

const LayOutPage = () => {
    return (
        <>
            <Header />
            <div>
                <Outlet />
            </div>
        </>
    );
};

export default LayOutPage;
