import { useSelector } from 'react-redux';
import useBreadcrumbs from './../../hooks/useBreadcrumbs';
import Breadcrumbs from './../../components/Breadcrumbs/Breadcrumbs';
import CheckoutForm from '../../components/CheckoutForm/CheckoutForm';

import styles from './Checkout.module.scss';

const Checkout = () => {
    const pathParts = useBreadcrumbs()
    const productsInCart = useSelector((state) => state.cart.productsInCart);
 
    const subTotalPrice = productsInCart?.map((product) => product.amount * product.price).reduce((acc, currentValue) => acc + currentValue, 0);
    const shippingPrice = parseFloat((subTotalPrice * 0.07).toFixed(2));
    const savingsPrice = parseFloat((subTotalPrice * 0.02).toFixed(2));
    const grandTotalPrice = subTotalPrice - savingsPrice + shippingPrice;

    const priceDetails = {
        subTotalPrice,
        shippingPrice,
        savingsPrice,
        total: grandTotalPrice
    };
      const orders = productsInCart.map((product) => {
          return {
              id: product.id,
              name: product.name,
              price: product.price,
              articul: product.articul,
              amount: product.amount
          };
      });
    
    const orderSummary = productsInCart?.map((product) => {
        return (
            <div className={styles.itemProduct} key={product.id}>
                <div className={styles.productData}>
                    <img src={product.imageUrl} alt={product.name} />
                    <div className={styles.productInfo}>
                        <h4>
                            {product.name} <span className={styles.productAmount}>x {product.amount}</span>
                        </h4>
                        <div className={styles.prodInfo}>
                            <div>
                                Color : <span className={styles.productColor}>{product.color}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={styles.priceProduct}>${product.price}</div>
            </div>
        );
    });
    return (
        <div className="wrapper">
            {<Breadcrumbs pathParts={pathParts} />}
            <h3 className={styles.titleCheckout}>Check Out</h3>
            <p className={styles.subTitleCheckout}>Billing Details</p>
            <div className={styles.checkoutWrapper}>
                <CheckoutForm orders={orders} priceDetails={priceDetails} />
                <div className={styles.orderSummary}>
                    <h4 className={styles.summaryTitle}>Order Summary</h4>
                    <div className={styles.orderList}>
                        {orderSummary}
                        <ul className={styles.priceDetails}>
                            <li className={styles.discountPrice}>
                                <div className={styles.subtotal}>
                                    <p>
                                        Subtotal <span className={styles.amountProducts}>( {productsInCart.length} items )</span>
                                    </p>
                                    <span>${subTotalPrice}</span>
                                </div>
                                <div className={styles.discount}>
                                    Savings <span>-${savingsPrice}</span>
                                </div>
                            </li>
                            <li>
                                Shipping <span>${shippingPrice}</span>
                            </li>
                            <li>
                                Total <span> ${grandTotalPrice}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Checkout;
