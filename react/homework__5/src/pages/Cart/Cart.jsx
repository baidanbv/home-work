import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import useBreadcrumbs from '../../hooks/useBreadcrumbs';
import { PropTypes } from 'prop-types';
import Breadcrumbs from '../../components/Breadcrumbs/Breadcrumbs';

import { RiDeleteBin6Line } from 'react-icons/ri';
import { MdKeyboardArrowUp, MdOutlineKeyboardArrowDown } from 'react-icons/md';

import { modalRemoveFromCartAC } from '../../redux/reducers/modal-reducer';
import { productToRemoveAC } from '../../redux/reducers/cart-reducer';

import styles from './Cart.module.scss';

const Cart = ({ amountIncrease, amountDecrease }) => {

    const pathParts = useBreadcrumbs()
    const dispatch = useDispatch();
    const productsInCart = useSelector((state) => state.cart.productsInCart);

    const handleModalRemoveFromCart = (productId) => {
        dispatch(productToRemoveAC(productId));
        dispatch(modalRemoveFromCartAC(true));
    };
    const subTotalPrice = productsInCart?.map((product) => product.amount * product.price).reduce((acc, currentValue) => acc + currentValue, 0);
    const shippingPrice = parseFloat((subTotalPrice * 0.07).toFixed(2));
    const grandTotalPrice = subTotalPrice + shippingPrice;
    
    const renderProducts = productsInCart?.map((product) => {
        const totalPrice = product.amount * product.price;

        return (
            <div className={styles.cartProduct} key={product.id}>
                <div className={styles.productData}>
                    <img src={product.imageUrl} alt={product.name} />
                    <div className={styles.productInfo}>
                        <h4>{product.name}</h4>
                        <div className={styles.prodInfo}>
                            <div>{product.color}</div>
                            <div>{product.articul}</div>
                        </div>
                    </div>
                </div>
                <div className={styles.productOtherData}>
                    <ul>
                        <li className={styles.bold}>${product.price}</li>
                        <li className={styles.totalPrice}>{totalPrice}$</li>
                        <li className={styles.bold}>
                            <div className={styles.counterProduct}>
                                <div className={styles.amount}>{product.amount}</div>
                                <div className={styles.changeAmount}>
                                    <span>
                                        <MdKeyboardArrowUp className={styles.countCart} onClick={() => amountIncrease(product)} />
                                    </span>
                                    <span>
                                        <MdOutlineKeyboardArrowDown className={styles.countCart} onClick={() => amountDecrease(product)} />
                                    </span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <RiDeleteBin6Line className={styles.btnDelete} onClick={() => handleModalRemoveFromCart(product.id)} />
                        </li>
                    </ul>
                </div>
            </div>
        );
    });
    return (
        <div>
            <div className="wrapper">
                {<Breadcrumbs pathParts={pathParts} />}
                <div className={styles.cartSubInfo}>
                    <p>Please fill in the fields below and click place order to complete your purchase!</p>
                    <p>
                        Already registered?
                        <Link to="/" className={styles.toLogin}>
                            Please login here
                        </Link>
                    </p>
                </div>
            </div>
            {productsInCart.length > 0 ? (
                <>
                    <div className={styles.productDetailsBg}>
                        <div className={`wrapper ${styles.productDetails}`}>
                            <span>Product Details</span>
                            <ul className={styles.detailsData}>
                                <li>Price</li>
                                <li>Subtotal</li>
                                <li>Quantity</li>
                                <li>action</li>
                            </ul>
                        </div>
                    </div>
                    <div className="wrapper">{renderProducts}</div>
                    <div className={styles.checkoutDetails}>
                        <div className={`wrapper ${styles.wrapperCheckoutDetails}`}>
                            <div className={styles.discountCodes}>
                                <h3>Discount Codes</h3>
                                <p>Enter your coupon code if you have one</p>
                                <div className={styles.applyCoupon}>
                                    <input type="text" />
                                    <button>Apply Coupon</button>
                                </div>
                                <Link to=".." relative="path">
                                    <button className={styles.continueShop}>Continue Shopping</button>
                                </Link>
                            </div>
                            <div className={styles.billingDetails}>
                                <ul className={styles.dataPrice}>
                                    <li>
                                        <span>Sub Total</span>
                                        <span className={styles.value}>${subTotalPrice}</span>
                                    </li>
                                    <li>
                                        <span>Shipping</span>
                                        <span className={styles.value}>${shippingPrice}</span>
                                    </li>
                                    <li>
                                        <span>Grand Total</span>
                                        <span className={styles.value}>${grandTotalPrice}</span>
                                    </li>
                                </ul>
                                <Link to="checkout" className={styles.checkoutBtn}>
                                    Proceed To Checkout
                                </Link>
                            </div>
                        </div>
                    </div>
                </>
            ) : (
                <div className={`wrapper ${styles.emptyCart}`}>Your cart is empty</div>
            )}
        </div>
    );
};

Cart.propTypes = {
    amountIncrease: PropTypes.func,
    amountDecrease: PropTypes.func
};

export default Cart;
