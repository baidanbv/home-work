import { composeWithDevTools } from '@redux-devtools/extension';
import { applyMiddleware, combineReducers, legacy_createStore as createStore } from 'redux';
import thunk from 'redux-thunk';
import productsReducer from '../reducers/products-reducer';
import modalReducer from '../reducers/modal-reducer';
import cartReducer from '../reducers/cart-reducer';
import favoritesReducer from '../reducers/favorites-reducer';
const rootReducer = combineReducers({
  products: productsReducer,
  modal: modalReducer,
  cart: cartReducer,
  favorites: favoritesReducer,
});
export const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));
