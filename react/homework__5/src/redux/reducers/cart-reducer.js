const PRODUCT_TO_ADD = 'PRODUCT_TO_ADD';
const PRODUCT_TO_REMOVE = 'PRODUCT_TO_REMOVE';
const PRODUCTS_IN_CART = 'PRODUCTS_IN_CART';
const CLEAR_AFTER_CHECKOUT = 'CLEAR_AFTER_CHECKOUT';
const productsInCart = JSON.parse(localStorage.getItem('productsInCart'));

const initialState = {
  productToAdd: null,
  productToRemove: null,
  productsInCart: productsInCart || [],
};

const cartReducer = (state = initialState, action) => {
  switch (action.type) {
      case PRODUCT_TO_ADD:
          return {
              ...state,
              productToAdd: action.payload
          };
      case PRODUCT_TO_REMOVE:
          return {
              ...state,
              productToRemove: action.payload
          };
      case PRODUCTS_IN_CART:
          return {
              ...state,
              productsInCart: action.payload
          };
      case CLEAR_AFTER_CHECKOUT:
          return {
              ...state,
              productsInCart: action.payload
          };
      default:
          return state;
  }
};

export const productToAddAC = (product) => ({
  type: PRODUCT_TO_ADD,
  payload: product,
});
export const productToRemoveAC = (product) => ({
  type: PRODUCT_TO_REMOVE,
  payload: product,
});
export const productsInCartAC = (product) => ({
  type: PRODUCTS_IN_CART,
  payload: product,
});
export const clearCartStorageAC = (product) => ({
    type: CLEAR_AFTER_CHECKOUT,
    payload: product
});
export default cartReducer;
