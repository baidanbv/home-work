import Slider from 'react-slick';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import styles from './BannerSlider.module.scss';
import { bannerSlidesArray } from './../../helpers/bannerSlidesArray';

const BannerSlider = () => {

    const SlickArrowLeft = ({ currentSlide, slideCount, ...props }) => (
        <button {...props} className={'slick-prev slick-arrow' + (currentSlide === 0 ? ' slick-disabled' : '')} aria-hidden="true" aria-disabled={currentSlide === 0 ? true : false} type="button">
            Previous
        </button>
    );
    const SlickArrowRight = ({ currentSlide, slideCount, ...props }) => (
        <button {...props} className={'slick-next slick-arrow' + (currentSlide === slideCount - 1 ? ' slick-disabled' : '')} aria-hidden="true" aria-disabled={currentSlide === slideCount - 1 ? true : false} type="button">
            Next
        </button>
    );
    const SliderSettings = {
        dots: true,
        infinite: true,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        prevArrow: <SlickArrowLeft />,
        nextArrow: <SlickArrowRight />
    };

    return (
        <Slider {...SliderSettings} className={styles.bannerSlider}>
            {bannerSlidesArray.map((slide) => {
                return (
                    <div className={styles.slide} key={slide.id}>
                        <img src={slide.imgUrl} alt={slide.title} />
                        <div className={styles.bannerData}>
                            <p className={styles.bannerCategory}>{slide.category}</p>
                            <h3 className={styles.bannerTitle}>{slide.title}</h3>
                            <h3 className={styles.bannerDescription}>{slide.description}</h3>
                        </div>
                    </div>
                );
            })}
        </Slider>
    );
};

export default BannerSlider;
