import styles from './Modal.module.scss';
import { MdClear } from 'react-icons/md';
import { PropTypes } from 'prop-types';

const Modal = ({ header, text, isCloseButton, closeHandler, actions }) => {
  const closeModalOutSide = (e) => {
    if (e.target.id === 'overlayPopup') {
      closeHandler();
    }
  };
  return (
    <div id='overlayPopup' className={styles.popupOverlay} onClick={closeModalOutSide}>
      <div className={styles.popup}>
        <div className={styles.popupHeader}>
          {header}
          {isCloseButton && (
            <MdClear className={styles.closeIcon} onClick={closeHandler} />
          )}
        </div>
        <div className={styles.popupBody}>{text}</div>
        <div className={styles.wrapperBtns}>{actions}</div>
      </div>
    </div>
  );
};

Modal.propTypes = {
  header: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  isCloseButton: PropTypes.bool.isRequired,
  closeHandler: PropTypes.func.isRequired,
  actions: PropTypes.object.isRequired,
};
export default Modal;
