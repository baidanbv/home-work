import { useSelector } from 'react-redux';
import SingleProduct from './SingleProduct/SingleProduct';
import styles from './Products.module.scss';
import BannerSlider from '../BannerSlider/BannerSlider';

const Products = () => {
    const { products } = useSelector((state) => state.products);
    const favoritesProducts = useSelector((state) => state.favorites.favoritesInCart);

    const listOfMenProducts = products
        .filter((item) => !item.isFemale)
        .map((item) => {
            const isFavorite = favoritesProducts.includes(item.id);

            return <SingleProduct dataProduct={item} key={item.id} isFavorite={isFavorite} />;
        });

    const listOfWomenProducts = products
        .filter((item) => item.isFemale)
        .map((item) => {
            const isFavorite = favoritesProducts.includes(item.id);

            return <SingleProduct dataProduct={item} key={item.id} isFavorite={isFavorite} />;
        });

  

    return (
        <>
           <BannerSlider/>
            <div className="wrapper">
                <section className={styles.productsSection}>
                    <h2 className="titleSection">Products For Men</h2>
                    <div className={styles.products}>{listOfMenProducts}</div>
                </section>
                <section className={styles.productsSection}>
                    <h2 className="titleSection">Products For Women</h2>
                    <div className={styles.products}>{listOfWomenProducts}</div>
                </section>
            </div>
        </>
    );
};

export default Products;
