import { useDispatch, useSelector } from 'react-redux';
import { PropTypes } from 'prop-types';
import { LuShoppingCart } from 'react-icons/lu';
import { AiFillHeart, AiOutlineHeart } from 'react-icons/ai';

import { modalAddToCartAC } from '../../../redux/reducers/modal-reducer';
import { productToAddAC } from '../../../redux/reducers/cart-reducer';
import { toggleFavoritesAC } from '../../../redux/reducers/favorites-reducer';

import styles from './SingleProduct.module.scss';

const SingleProduct = ({ dataProduct, isFavorite }) => {
    const favoritesProducts = useSelector((state) => state.favorites.favoritesInCart);
    const dispatch = useDispatch();

    const handleModalAddToCard = (product) => {
        dispatch(productToAddAC(product));
        dispatch(modalAddToCartAC(true));
    };

    const toggleFavorite = (productId) => {
        const currentFavorites = favoritesProducts.includes(productId) ? favoritesProducts.filter((id) => id !== productId) : [...favoritesProducts, productId];
        dispatch(toggleFavoritesAC(currentFavorites));
    };

    return (
        <div className={styles.itemProduct}>
            <div className={styles.productImg}>
                <img src={dataProduct.imageUrl} alt={dataProduct.name} />
            </div>
            <h4 className={styles.productTitle}>{dataProduct.name}</h4>
            <div className={styles.productInfo}>
                <div className={styles.productPrice}>
                    <span>Price:</span>
                    <span>{dataProduct.price}$</span>
                </div>
                <div className={styles.productColor}>
                    <span>Color:</span>
                    <span style={{ background: dataProduct.color }}>{dataProduct.color}</span>
                </div>
                <div className={styles.productBtns}>
                    <span className={styles.productButton} onClick={() => handleModalAddToCard(dataProduct)}>
                        <LuShoppingCart className={styles.addToCardBtn} />
                    </span>
                    <span className={styles.productButton} onClick={() => toggleFavorite(dataProduct.id)}>
                        {!isFavorite && <AiOutlineHeart className={styles.addToFavoritesBtn} />}
                        {isFavorite && <AiFillHeart className={styles.addToFavoritesBtn} />}
                    </span>
                </div>
            </div>
        </div>
    );
};
SingleProduct.propTypes = {
    dataProduct: PropTypes.shape({
        name: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        imageUrl: PropTypes.string.isRequired,
        articul: PropTypes.number.isRequired,
        color: PropTypes.string,
        isFavorite: PropTypes.bool.isRequired
    }),
};

SingleProduct.defaultProps = {
    dataProduct: PropTypes.shape({
        color: ''
    })
};
export default SingleProduct;
