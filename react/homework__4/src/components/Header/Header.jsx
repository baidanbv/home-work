import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { LuShoppingCart } from 'react-icons/lu';
import { AiOutlineHeart } from 'react-icons/ai';
import { ReactComponent as Logo } from './icons/Logo.svg';
import NavBar from '../NavBar/NavBar';
import Search from '../Search/Search';
import styles from './Header.module.scss';

const Header = () => {
    const productsInCartRedux = useSelector((state) => state.cart.productsInCart);
    const favoritesProducts = useSelector((state) => state.favorites.favoritesInCart);

    const quantityInCard = productsInCartRedux.reduce((acc, currentValue) => acc + currentValue.amount, 0);
    const quantityInFavorite = favoritesProducts.length;

    return (
        <header className={`wrapper ${styles.header}`}>
            <Link to="/">
                <Logo />
            </Link>
            <NavBar />
            <Search />
            <div className={styles.headerShopBtns}>
                <Link to="shop/favorites">
                    <span className={styles.headerShopBtn}>
                        <AiOutlineHeart />
                        <span className={styles.cardCounter}>{quantityInFavorite > 0 && quantityInFavorite}</span>
                    </span>
                </Link>
                <Link to="shop/cart">
                    <span className={styles.headerShopBtn}>
                        <LuShoppingCart />
                        <span className={styles.cardCounter}>{quantityInCard > 0 && quantityInCard}</span>
                    </span>
                </Link>
            </div>
        </header>
    );
};

export default Header;
