import { useSelector } from 'react-redux';

import SingleProduct from './SingleProduct/SingleProduct';
import styles from './Products.module.scss';

const Products = () => {
    const { products } = useSelector((state) => state.products);
    const favoritesProducts = useSelector((state) => state.favorites.favoritesInCart);

    const listOfProducts = products.map((item) => {
        const isFavorite = favoritesProducts.includes(item.id);

        return <SingleProduct dataProduct={item} key={item.id} isFavorite={isFavorite} />;
    });

    return (
        <div className="wrapper">
            <h2 className={styles.categoriesTitle}>Categories For Men</h2>
            <div className={styles.products}>{listOfProducts}</div>
        </div>
    );
};

export default Products;
