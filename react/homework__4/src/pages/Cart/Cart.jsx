import { useLocation, Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { PropTypes } from 'prop-types';

import { AiOutlineRight } from 'react-icons/ai';
import { RiDeleteBin6Line } from 'react-icons/ri';
import { MdKeyboardArrowUp, MdOutlineKeyboardArrowDown } from 'react-icons/md';

import { modalRemoveFromCartAC } from '../../redux/reducers/modal-reducer';
import { productToRemoveAC } from '../../redux/reducers/cart-reducer';

import styles from './Cart.module.scss';

const Cart = ({ amountIncrease, amountDecrease, }) => {
  const location = useLocation();
  const dispatch = useDispatch()
  const productsInCart = useSelector(state => state.cart.productsInCart)
  
  const pathParts = location.pathname.split('/').filter((part) => part !== '');
  const breadCrumbs = pathParts.map((item, index) => {
    if (index === pathParts.length - 1) {
      return (
        <span className="currentPage" key={index}>
          {item}
        </span>
      );
    }
    return (
      <Link to=".." relative="path" key={index}>
        {' '}
        {item} <AiOutlineRight />
      </Link>
    );
  });

  const handleModalRemoveFromCart = (productId) => {
    dispatch(productToRemoveAC(productId));
    dispatch(modalRemoveFromCartAC(true));
  };

  const renderProducts = productsInCart?.map((product) => {
    const totalPrice = product.amount * product.price;

    return (
      <div className={styles.cartProduct} key={product.id}>
        <div className={styles.productData}>
          <img src={product.imageUrl} alt={product.name} />
          <div className={styles.productInfo}>
            <h4>{product.name}</h4>
            <div className={styles.prodInfo}>
              <div>{product.color}</div>
              <div>{product.articul}</div>
            </div>
          </div>
        </div>
        <div className={styles.productOtherData}>
          <ul>
            <li className={styles.bold}>${product.price}</li>
            <li className={styles.totalPrice}>{totalPrice}$</li>
            <li className={styles.bold}>
              <div className={styles.counterProduct}>
                <div className={styles.amount}>{product.amount}</div>
                <div className={styles.changeAmount}>
                  <span>
                    <MdKeyboardArrowUp
                      className={styles.countCart}
                      onClick={() => amountIncrease(product)}
                    />
                  </span>
                  <span>
                    <MdOutlineKeyboardArrowDown
                      className={styles.countCart}
                      onClick={() => amountDecrease(product)}
                    />
                  </span>
                </div>
              </div>
            </li>
            <li>
              <RiDeleteBin6Line
                className={styles.btnDelete}
                onClick={() => handleModalRemoveFromCart(product.id)}
              />
            </li>
          </ul>
        </div>
      </div>
    );
  });
  return (
    <div>
      <div className="wrapper">
        <div className="breadCrumbs">
          <Link to="/">
            Home <AiOutlineRight />
          </Link>
          {breadCrumbs}
        </div>
        <div className={styles.cartSubInfo}>
          <p>
            Please fill in the fields below and click place order to complete
            your purchase!
          </p>
          <p>
            Already registered?{' '}
            <Link to="/" className={styles.toLogin}>
              Please login here
            </Link>
          </p>
        </div>
      </div>
      <div className={styles.productDetailsBg}>
        <div className={`wrapper ${styles.productDetails}`}>
          <span>Product Details</span>
          <ul className={styles.detailsData}>
            <li>Price</li>
            <li>Subtotal</li>
            <li>Quantity</li>
            <li>action</li>
          </ul>
        </div>
      </div>
      <div className="wrapper">{renderProducts}</div>
    </div>
  );
};

Cart.propTypes = {
    amountIncrease: PropTypes.func,
    amountDecrease: PropTypes.func
};

export default Cart;
