import styles from './Button.module.scss';
const Button = ({ backgroundColor, text, onModalHandler }) => {
  return (
    <button
      className={styles.showModal}
      onClick={onModalHandler}
      style={{ backgroundColor: backgroundColor, borderColor: backgroundColor }}
    >
      {text}
    </button>
  );
};

export default Button;
