import { useState, useEffect } from 'react';
import { Routes, Route } from 'react-router-dom';

import { sendRequest } from './helpers/sendRequest';

import Modal from './components/Popup/Popup';
import Home from './pages/Home/Home';
import LayOutPage from './pages/LayOutPage/LayOutPage';
import Shop from './pages/Shop/Shop';
import Cart from './pages/Cart/Cart';
import Favorites from './pages/Favorites/Favorites';
import './App.scss';
import Men from './pages/Men/Men';
import Women from './pages/Women/Women';
import Combos from './pages/Combos/Combos';
import Joggers from './pages/Joggers/Joggers';

function App() {
  const productsInCart = JSON.parse(localStorage.getItem('productsInCart'));
  const favoriteFromStorage = JSON.parse(
    localStorage.getItem('favoriteProductsId')
  );

  const [products, setProducts] = useState([]);
  const [isModalAddToCard, setIsModalAddToCard] = useState(false);
  const [isModalRemoveFromCart, setIsModalRemoveFromCart] = useState(false);
  const [addedProductId, setAddedProductId] = useState(0);
  const [productToRemove, setProductToRemove] = useState(0);
  const [cardsProductsId, setCardsProductsId] = useState(productsInCart || []);
  const [favoritesId, setFavoritesId] = useState(favoriteFromStorage || []);
  
  const quantityInCard = cardsProductsId.reduce(
    (acc, currentValue) => acc + currentValue.amount,
    0
  );

  const quantityInFavorite = favoritesId.length;

  const handleModalAddToCard = (product) => {
    setIsModalAddToCard(!isModalAddToCard);
    setAddedProductId(product);
  };

  const handleModalRemoveFromCart = (product) => {
    setIsModalRemoveFromCart(!isModalRemoveFromCart);
    setProductToRemove(product);
  };

  const closeHandler = () => {
    if (isModalAddToCard) {
      setIsModalAddToCard(false);
    } else if (isModalRemoveFromCart) {
      setIsModalRemoveFromCart(false);
    }
  };

  const addToCard = (productId) => {
    setCardsProductsId((prev) => {
      const currentProduct = prev.find((item) => item.id === productId.id);
      if (currentProduct) {
        return prev.map((item) =>
          item.id === productId.id ? { ...item, amount: item.amount + 1 } : item
        );
      } else {
        return [...prev, { ...productId, amount: productId.amount + 1 }];
      }
    });
    setIsModalAddToCard(false);
  };

  const removeFromCart = (productId) => {
    setCardsProductsId((prev) => {
      const currentProduct = prev.find((item) => item.id === productId);
      if (currentProduct) {
        return prev.filter((item) => item.id !== productId);
      } else {
        return [...prev, productsInCart];
      }
    });
    setIsModalRemoveFromCart(!isModalRemoveFromCart);
  };

  const amountDecrease = (product) => {
    setCardsProductsId((prev) => {
      const currentProduct = prev.find((item) => item.id === product.id);
      const currentAmount = prev.find((item) => item.amount === product.amount);

      if (currentAmount.amount === 1) {
        return prev;
      }

      if (currentProduct) {
        return prev.map((item) =>
          item.id === product.id ? { ...item, amount: item.amount - 1 } : item
        );
      } else {
        return [...prev, { ...product, amount: product.amount - 1 }];
      }
    });
  };
  const toggleFavorite = (productId) => {
    const currentFavorites = favoritesId.includes(productId)
      ? favoritesId.filter((id) => id !== productId)
      : [...favoritesId, productId];

    setFavoritesId(currentFavorites);
  };

  useEffect(() => {
    sendRequest('/products.json').then((products) => {
      setProducts(products);
    });
  }, []);

  useEffect(() => {
    localStorage.setItem('productsInCart', JSON.stringify(cardsProductsId));
  }, [cardsProductsId]);

  useEffect(() => {
    localStorage.setItem('favoriteProductsId', JSON.stringify(favoritesId));
  }, [favoritesId]);

  return (
    <div className="App">
      <Routes>
        <Route
          path="/"
          element={
            <LayOutPage
              quantityInCard={quantityInCard}
              quantityInFavorite={quantityInFavorite}
            />
          }
        >
          <Route index element={<Home />} />
          <Route path="men" element={<Men />} />
          <Route path="women" element={<Women />} />
          <Route path="combos" element={<Combos />} />
          <Route path="joggers" element={<Joggers />} />
          <Route
            path="shop"
            element={
              <Shop
                products={products}
                handleModalAddToCard={handleModalAddToCard}
                toggleFavorite={toggleFavorite}
                favoritesId={favoritesId}
              />
            }
          />
          <Route
            path="shop/cart"
            element={
              <Cart
                productsInCart={cardsProductsId}
                removeFromCart={removeFromCart}
                handleModalRemoveFromCart={handleModalRemoveFromCart}
                amountIncrease={addToCard}
                amountDecrease={amountDecrease}
              />
            }
          />
          <Route
            path="shop/favorites"
            element={
              <Favorites
                products={products}
                favoritesId={favoritesId}
                addToCard={addToCard}
                toggleFavorite={toggleFavorite}
              />
            }
          />
        </Route>
      </Routes>
      {isModalAddToCard && (
        <Modal
          header="Add to Card"
          text="Are you sure at you want to add this product to your card?"
          isCloseButton
          closeHandler={closeHandler}
          actions={
            <>
              <button onClick={() => addToCard(addedProductId)}>Add</button>
              <button>Cancel</button>
            </>
          }
        />
      )}
      {isModalRemoveFromCart && (
        <Modal
          header="Remove from Card"
          text="Are you sure at you want remove this product from your card?"
          isCloseButton
          closeHandler={closeHandler}
          actions={
            <>
              <button onClick={() => removeFromCart(productToRemove)}>
                Remove
              </button>
              <button>Cancel</button>
            </>
          }
        />
      )}
    </div>
  );
}

export default App;
