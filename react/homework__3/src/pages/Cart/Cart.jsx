import { useLocation, Link } from 'react-router-dom';

import styles from './Cart.module.scss';
import { AiOutlineRight } from 'react-icons/ai';
import { RiDeleteBin6Line } from 'react-icons/ri';
import { MdKeyboardArrowUp, MdOutlineKeyboardArrowDown } from 'react-icons/md';

const Cart = ({
  productsInCart,
  handleModalRemoveFromCart,
  amountIncrease,
  amountDecrease,
}) => {
  const location = useLocation();

  const pathParts = location.pathname.split('/').filter((part) => part !== '');
  const breadCrumbs = pathParts.map((item, index) => {
    if (index === pathParts.length - 1) {
      return (
        <span className="currentPage" key={index}>
          {item}
        </span>
      );
    }
    return (
      <Link to=".." relative="path" key={index}>
        {' '}
        {item} <AiOutlineRight />
      </Link>
    );
  });

  const renderProducts = productsInCart?.map((product) => {
    const totalPrice = product.amount * product.price;

    return (
      <div className={styles.cartProduct} key={product.id}>
        <div className={styles.productData}>
          <img src={product.imageUrl} alt={product.name} />
          <div className={styles.productInfo}>
            <h4>{product.name}</h4>
            <div className={styles.prodInfo}>
              <div>{product.color}</div>
              <div>{product.articul}</div>
            </div>
          </div>
        </div>
        <div className={styles.productOtherData}>
          <ul>
            <li className={styles.bold}>${product.price}</li>
            <li className={styles.totalPrice}>{totalPrice}$</li>
            <li className={styles.bold}>
              <div className={styles.counterProduct}>
                <div className={styles.amount}>{product.amount}</div>
                <div className={styles.changeAmount}>
                  <span>
                    <MdKeyboardArrowUp
                      className={styles.countCart}
                      onClick={() => amountIncrease(product)}
                    />
                  </span>
                  <span>
                    <MdOutlineKeyboardArrowDown
                      className={styles.countCart}
                      onClick={() => amountDecrease(product)}
                    />
                  </span>
                </div>
              </div>
            </li>
            <li>
              <RiDeleteBin6Line
                className={styles.btnDelete}
                onClick={() => handleModalRemoveFromCart(product.id)}
              />
            </li>
          </ul>
        </div>
      </div>
    );
  });
  return (
    <div>
      <div className="wrapper">
        <div className="breadCrumbs">
          <Link to="/">
            Home <AiOutlineRight />
          </Link>
          {breadCrumbs}
        </div>
        <div className={styles.cartSubInfo}>
          <p>
            Please fill in the fields below and click place order to complete
            your purchase!
          </p>
          <p>
            Already registered?{' '}
            <Link to="/" className={styles.toLogin}>
              Please login here
            </Link>
          </p>
        </div>
      </div>
      <div className={styles.productDetailsBg}>
        <div className={`wrapper ${styles.productDetails}`}>
          <span>Product Details</span>
          <ul className={styles.detailsData}>
            <li>Price</li>
            <li>Subtotal</li>
            <li>Quantity</li>
            <li>action</li>
          </ul>
        </div>
      </div>
      <div className="wrapper">{renderProducts}</div>
    </div>
  );
};

export default Cart;
