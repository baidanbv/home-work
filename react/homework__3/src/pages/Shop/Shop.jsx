import Products from "../../components/Products/Products";

const Shop = ({ products, handleModalAddToCard, toggleFavorite, favoritesId }) => {
  return (
    <>
      <Products
        products={products}
        handleModalAddToCard={handleModalAddToCard}
        toggleFavorite={toggleFavorite}
        favoritesId={favoritesId}
      />
    </>
  );
};

export default Shop