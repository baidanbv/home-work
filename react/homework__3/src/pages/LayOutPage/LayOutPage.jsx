import { Outlet } from "react-router-dom";
import Header from "../../components/Header/Header";

const LayOutPage = ({ quantityInCard, quantityInFavorite }) => {
  return (
    <>
      <Header
        quantityInCard={quantityInCard}
        quantityInFavorite={quantityInFavorite}
      />
      <div>
        <Outlet />
      </div>
    </>
  );
};

export default LayOutPage