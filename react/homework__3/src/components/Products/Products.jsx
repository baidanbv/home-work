import PropTypes from 'prop-types';
import styles from './Products.module.scss';
import SingleProduct from './SingleProduct/SingleProduct';

const Products = ({
  products,
  handleModalAddToCard,
  toggleFavorite,
  favoritesId,
}) => {

  const listOfProducts = products.map((item) => {
    const isFavorite = favoritesId.includes(item.id);
    return (
      <SingleProduct
        dataProduct={item}
        key={item.id}
        handleModal={handleModalAddToCard}
        toggleFavorite={toggleFavorite}
        isFavorite={isFavorite}
      />
    );
  });

  return (
    <div className="wrapper">
      <h2 className={styles.categoriesTitle}>Categories For Men</h2>
      <div className={styles.products}>{listOfProducts}</div>
    </div>
  );
};

Products.propTypes = {
  products: PropTypes.array.isRequired,
  handleModalAddToCard: PropTypes.func.isRequired,
  toggleFavorite: PropTypes.func.isRequired,
  favoritesId: PropTypes.array.isRequired,
};

export default Products;
