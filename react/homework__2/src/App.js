import { useState, useEffect } from 'react';
import './App.scss';
import { sendRequest } from './helpers/sendRequest';
import Products from './components/Products/Products';
import Header from './components/Header/Header';
import Modal from './components/Popup/Popup';

function App() {
  const productsInCart = JSON.parse(localStorage.getItem('productsInCart'));
  const favoriteFromStorage = JSON.parse(localStorage.getItem('favoriteProductsId'));

  const [products, setProducts] = useState([]);
  const [isModalAddToCard, setIsModalAddToCard] = useState(false);
  const [addedProductId, setAddedProductId] = useState(0);
  const [cardsProductsId, setCardsProductsId] = useState(productsInCart || []);
  const [favoritesId, setfavoritesId] = useState(favoriteFromStorage || []);

  const quantityInCard = cardsProductsId.length;
  const quantityInFavorite = favoritesId.length;

  localStorage.setItem('productsInCart', JSON.stringify(cardsProductsId));
  localStorage.setItem('favoriteProductsId', JSON.stringify(favoritesId));

  
  const handleModalAddToCard = (id) => {
    setIsModalAddToCard(!isModalAddToCard);
    setAddedProductId(id);
  };

  const closeHandler = () => {
    setIsModalAddToCard(false);
  };

  const addToCard = (productId) => {
    setCardsProductsId([...cardsProductsId, productId]);
    setIsModalAddToCard(false);
  };

  const toggleFavorite = (productId) => {
    const currentFavorites = favoritesId.includes(productId) ? favoritesId.filter(id => id !== productId) : [...favoritesId, productId]
    setfavoritesId(currentFavorites)
  };

  useEffect(() => {
    sendRequest('/products.json').then((products) => {
      setProducts(products);
    });
  }, []);

  return (
    <div className="App">
      <Header quantityInCard={quantityInCard} favorite={quantityInFavorite} />
      <Products
        products={products}
        handleModal={handleModalAddToCard}
        toggleFavorite={toggleFavorite}
        storageFavorite={favoritesId}
      />
      {isModalAddToCard && (
        <Modal
          header="Add to Card"
          text="Are you sure at you want to add this product to your card?"
          isCloseButton
          closeHandler={closeHandler}
          actions={
            <>
              <button onClick={() => addToCard(addedProductId)}>Add</button>
              <button>Cancel</button>
            </>
          }
        />
      )}
    </div>
  );
}

export default App;
