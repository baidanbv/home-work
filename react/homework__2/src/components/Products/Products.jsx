import PropTypes from 'prop-types';
import styles from './Products.module.scss';
import SingleProduct from './SingleProduct/SingleProduct';
const Products = ({ products, handleModal, toggleFavorite, storageFavorite }) => {
   
  const listOfProducts = products.map((item) => {
        const isFavorite = storageFavorite.includes(item.id)
        return <SingleProduct dataProduct={item} key={item.id} handleModal={handleModal} toggleFavorite={toggleFavorite} isFavorite={ isFavorite} />
     });
   
  return (
    <div className="wrapper">
      <h2 className={styles.categoriesTitle}>Categories For Men</h2>
      <div className={styles.products}>{listOfProducts}</div>
    </div>
  );
};

Products.propTypes = {
  products: PropTypes.array.isRequired,
  handleModal: PropTypes.func.isRequired,
  toggleFavorite: PropTypes.func.isRequired,
  storageFavorite: PropTypes.array.isRequired,
};

export default Products;
