import { PropTypes } from 'prop-types';
import { LuShoppingCart } from 'react-icons/lu';
import { AiFillHeart, AiOutlineHeart } from 'react-icons/ai';
import styles from './SingleProduct.module.scss';

const SingleProduct = ({ dataProduct, handleModal, toggleFavorite, isFavorite }) => {
 
  return (
    <div className={styles.itemProduct}>
      <div className={styles.productImg}>
        <img src={dataProduct.imageUrl} alt={dataProduct.name} />
      </div>
      <h4 className={styles.productTitle}>{dataProduct.name}</h4>
      <div className={styles.productInfo}>
        <div className={styles.productPrice}>
          <span>Price:</span>
          <span>{dataProduct.price}$</span>
        </div>
        <div className={styles.productColor}>
          <span>Color:</span>
          <span style={{ background: dataProduct.color }}>
            {dataProduct.color}
          </span>
        </div>
        <div className={styles.productBtns}>
          <span
            className={styles.productButton}
            onClick={() => handleModal(dataProduct.id)}
          >
            <LuShoppingCart className={styles.addToCardBtn} />
          </span>
          <span
            className={styles.productButton}
            onClick={() => toggleFavorite(dataProduct.id)}
          >
            {!isFavorite && (
              <AiOutlineHeart className={styles.addToFavoritesBtn} />
            )}
            {isFavorite && <AiFillHeart className={styles.addToFavoritesBtn} />}
          </span>
        </div>
      </div>
    </div>
  );
};
SingleProduct.propTypes = {
  dataProduct: PropTypes.shape({
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    imageUrl: PropTypes.string.isRequired,
    articul: PropTypes.number.isRequired,
    color: PropTypes.string,
    isFavorite: PropTypes.bool.isRequired,
  }),
  handleModal: PropTypes.func.isRequired,
  toggleFavorite: PropTypes.func.isRequired,
};

SingleProduct.defaultProps = {
  dataProduct: PropTypes.shape({
    color: '',
  }),
};
export default SingleProduct;
