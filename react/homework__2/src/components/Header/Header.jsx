import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import NavBar from '../NavBar/NavBar';
import Search from '../Search/Search';
import { ReactComponent as Logo } from './icons/Logo.svg';
import { LuShoppingCart } from 'react-icons/lu';
import { AiOutlineHeart } from 'react-icons/ai';
import styles from './Header.module.scss';


const Header = ({ quantityInCard = 0, favorite = 0 }) => {
  return (
    <header className={`wrapper ${styles.header}`}>
      <Link to="/">
        <Logo />
      </Link>
      <NavBar />
      <Search />
      <div className={styles.headerShopBtns}>
        <span className={styles.headerShopBtn}>
          <AiOutlineHeart />
          <span className={styles.cardCounter}>{favorite > 0 && favorite}</span>
        </span>
        <span className={styles.headerShopBtn}>
          <LuShoppingCart />
          <span className={styles.cardCounter}>
            {quantityInCard > 0 && quantityInCard}
          </span>
        </span>
      </div>
    </header>
  );
};
Header.propTypes = {
  quantityInCard: PropTypes.number,
  favorite: PropTypes.number,
};
export default Header;
